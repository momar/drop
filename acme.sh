#!/bin/sh
command="$1"
[ -n "$command" ] || exec acme.sh
shift
[ -e /root/.acme.sh ] || ln -s /data/acme /root/.acme.sh
exec /usr/bin/acme.sh "$command" "$@" --reloadcmd '
  for f in /data/*.*/; do
    [ -f ${f}/fullchain.cer ] && cat ${f}*.key ${f}fullchain.cer /data/dhparam.pem > /data/bundles/$(basename $f).pem;
  done;
  killall -HUP hitch;
  restart_containers=$(docker ps --filter label=drop.restart --format "{{ .ID }}" --no-trunc);
  [ -z "$restart_containers" ] || docker restart $restart_containers;
  reload_containers=$(docker ps --filter label=drop.restart --format "{{ .ID }}" --no-trunc);
  [ -z "$reload_containers" ] || docker kill -sHUP $reload_containers;'
