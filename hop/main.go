package main

func main() {

	// initialize the reverse proxy
	proxy := &Proxy{ Resolvers: []Resolver{ &ResolveDocker{} }}
	go proxy.Listen()

	// initialize the HTTP server for ACME challenges & HTTPS redirects
	redirect := &Redirect{ Webroot:  "/var/www" }
	go redirect.Listen()

	// wait for an infinite amount of time
	select{}

}
