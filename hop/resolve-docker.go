package main

import (
	"context"
	"math/rand"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
	"net"
	"strings"
	"sync"
	"time"
)

// ResolveDocker represents an upstream target resolver based on Docker container labels.
type ResolveDocker struct{
	cache map[string]*dockerCache
	client *client.Client
}

// Init initializes the cache and the Docker client connection.
func (resolver *ResolveDocker) Init() error {
	var err error
	resolver.client, err = client.NewEnvClient()
	if err != nil {
		return err
	}

	resolver.cache = map[string]*dockerCache{}
	go func() {
		for {
			// clean up expired cache entries every 5 minutes
			now := time.Now()
			for key, entry := range resolver.cache {
				if now.Sub(entry.Creation) > dockerCacheExpiration {
					delete(resolver.cache, key)
				}
			}
			time.Sleep(time.Duration(1.5 * float64(dockerCacheExpiration)))
		}
	}()
	return nil
}

// Resolve returns the upstream target of a container with the specified domain as its drop.domain label, or an empty
// string if no such container could be found.
func (resolver *ResolveDocker) Resolve(domain string) string {
	if entry, ok := resolver.cache[domain]; ok {
		// if an API request is currently running, wait until it's completed
		entry.Working.RLock()
		entry.Working.RUnlock()

		// if the last TCP connection check was less than 30 seconds ago, assume that it's still valid
		if time.Now().Sub(entry.LastCheck) < 30 * time.Second {
			return entry.Upstream
		}

		// connect via TCP to check if the upstream target is still reachable
		entry.LastCheck = time.Now()
		conn, err := net.Dial("tcp", entry.Upstream)
		if err == nil {
			conn.Close()
			return entry.Upstream
		}
	}

	// create a cache entry, and append it in a locked state
	entry := &dockerCache{
		Creation:  time.Now(),
		LastCheck: time.Time{},
		Upstream:  "",
		Working:   sync.RWMutex{},
	}
	entry.Working.Lock()
	entry.LastCheck = entry.Creation
	// make sure to always work with "entry", never with "resolver.cache[domain]" in this method, as the latter may
	// change due to simultaneous requests, and you might end up with a panic due to unlocking an already unlocked Mutex
	resolver.cache[domain] = entry

	// list the containers that match the domain
	containerFilters := filters.NewArgs()
	containerFilters.Add("label", "drop.domain=" + domain)
	containerList, err := resolver.client.ContainerList(context.Background(), types.ContainerListOptions{
		Filters: containerFilters,
	})
	if throw(err, false) {
		entry.Upstream = ""
		entry.Working.Unlock()
		return entry.Upstream
	}
	if len(containerList) < 1 {
		entry.Upstream = ""
		entry.Working.Unlock()
		return entry.Upstream
	}

	// inspect the container to get access to other labels (like drop.port) and exposed ports; this selects a random
	// container from the list, which is not really load balancing yet as the returned value will be cached
	containerDetails, err := resolver.client.ContainerInspect(context.Background(), containerList[rand.Int() % len(containerList)].ID)
	if throw(err, false) {
		entry.Upstream = ""
		entry.Working.Unlock()
		return entry.Upstream
	}

	// if a custom upstream is set, use that
	if host, _ := containerDetails.Config.Labels["drop.upstream"]; host != "" {
		if strings.Contains(host, ":") {
			entry.Upstream = host
			entry.Working.Unlock()
			return entry.Upstream
		} else if port, _ := containerDetails.Config.Labels["drop.port"]; port != "" {
			entry.Upstream = host + ":" + port
			entry.Working.Unlock()
			return entry.Upstream
		} else {
			entry.Upstream = host + ":80"
			entry.Working.Unlock()
			return entry.Upstream
		}
	}

	// get IP & port from the container's network settings
	if containerDetails.NetworkSettings == nil {
		return ""
	}
	firstNetwork := ""
	for network, _ := range containerDetails.NetworkSettings.Networks {
		firstNetwork = network
	}
	if firstNetwork == "" {
		entry.Upstream = "127.0.0.1:" + dockerPort(containerDetails.Config)
		entry.Working.Unlock()
		return entry.Upstream
	} else {
		entry.Upstream = containerDetails.NetworkSettings.Networks[firstNetwork].IPAddress + ":" + dockerPort(containerDetails.Config)
		entry.Working.Unlock()
		return entry.Upstream
	}
}

// dockerCache is a cache entry for an upstream target found via Docker container labels.
type dockerCache struct{
	Creation time.Time
	LastCheck time.Time
	Upstream string
	Working sync.RWMutex
}

// dockerCacheExpiration is the minimum validity of a dockerCache entry - the maximum validity will be 2.5 * dockerCacheExpiration.
const dockerCacheExpiration = 5 * time.Minute

// dockerPort returns the upstream port from a container by looking at the labels and exposed ports - the drop.port label
// will be preferred, then exposed ports starting with an 8, then ports starting with a 9, then the first port, then 80.
func dockerPort(config *container.Config) string {
	if port, _ := config.Labels["drop.port"]; port != "" {
		return port
	}
	port9 := ""
	portAny := ""
	for exposedPort := range config.ExposedPorts {
		port := exposedPort.Port()
		if port[0] == '8' {
			return port // we found a port starting with 8, let's use it!
		} else if port[0] == '9' && port9 == "" {
			port9 = port // we found a port starting with 9, use it later if we can't find one starting with 8
		} else if port9 != "" {
			break // we already found a port starting with 9, and that one isn't any better
		} else if portAny == "" {
			portAny = port // we found any port, use it later if we can't find one starting with 8 or 9
		}
	}
	if port9 != "" {
		return port9
	} else if portAny != "" {
		return portAny
	} else {
		return "80"
	}
}
