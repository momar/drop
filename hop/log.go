package main

import (
	"fmt"
	"io"
	"os"
	"runtime"
	"strconv"
	"time"
	"github.com/muesli/termenv"
)

// debug prints a message to the console, prefixed by the current date and time and the application name. The last
// argument might be an io.Writer which will be used to print the message.
func debug(msg string, v ...interface{}) {
	// select output writer
	var out io.Writer = os.Stdout
	if len(v) > 0 {
		if w, ok := v[len(v)-1].(io.Writer); ok {
			out = w
			v = v[:len(v)-1]
		}
	}

	// get caller information: https://stackoverflow.com/a/35213181
	programCounters := make([]uintptr, 2)
	n := runtime.Callers(2, programCounters)
	frame := runtime.Frame{Function: "unknown"}
	if n > 0 {
		frames := runtime.CallersFrames(programCounters[:n])
		frame, _ = frames.Next()
		if frame.Function == "main.throw" {
			frame, _ = frames.Next()
		}
	}
	caller := termenv.String(frame.Function + ":" + strconv.Itoa(frame.Line)).Foreground(termenv.ANSIColor(8)).String()

	// print the message
	fmt.Fprintf(out, "[%s] {hop} ", time.Now().Format(time.UnixDate))
	fmt.Fprintf(out, msg, v...)
	fmt.Fprintf(out, " %s\n", caller)
}

// throw prints an error to the console and returns true if it's non-nil, otherwise it just returns false; if "fatal" is
// set to true, a non-nil error will also cause os.Exit(1) to be called.
//
// Usage example:
// ```go
// if throw(err, false) {
//   return
// }
// ```
func throw(err error, fatal bool) bool {
	if err != nil {
		color := termenv.ANSIColor(3) // yellow
		if fatal {
			color = termenv.ANSIColor(1) // red
		}
		debug(termenv.String("%s").Bold().Foreground(color).String(), err.Error(), os.Stderr)
		if fatal {
			os.Exit(1)
		}
		return true
	}
	return false
}
