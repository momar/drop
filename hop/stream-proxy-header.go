package main

// ParseProxyHeader reads & parses the PROXY v1/v2 protocol (https://www.haproxy.org/download/1.8/doc/proxy-protocol.txt)
// from the stream connection to `stream.Data["ConnectionType|ClientIP|ServerIP|ClientPort|ServerPort|..."]`. It falls
// back to the actual connection details if no PROXY message is prepended.
func (stream *StreamContext) ParseProxyProtocol() error {
	// TODO
	return nil
}
