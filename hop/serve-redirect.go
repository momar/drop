package main

import (
	"errors"
	"net"
	"net/http"
	"os"
	"path"
	"strings"
)

// Redirect is a HTTP server that serves ACME challenges from the specified Webroot (if it's not an empty string) and
// permanently redirects everything else to HTTPS. It listens on the specified address, defaulting to [::]:80.
type Redirect struct{
	Webroot string
	Address string
	listener net.Listener
}

// Listen is a blocking method that starts the redirection server on the specified address and accepts connections using
// the `ServeHTTP` method.
func (redirect *Redirect) Listen() {
	if redirect.Address == "" {
		redirect.Address = "[::]:80"
	}

	redirect.Webroot = strings.TrimSuffix(path.Clean(redirect.Webroot), "/")
	if info, err := os.Stat(redirect.Webroot); redirect.Webroot != "" && (err != nil || !info.IsDir()) {
		redirect.Webroot = ""
		if !throw(err, false) {
			throw(errors.New("couldn't access ACME webroot"), false)
		}
	}

	debug("listening on " + redirect.Address + " for HTTP->HTTPS redirection and ACME validation")
	err := http.ListenAndServe(redirect.Address, redirect)
	throw(err, true)
}

// ServeHTTP is the handler for the HTTP server that responds either with the file from the Webroot.
func (redirect *Redirect) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Server", "hop")
	p := path.Clean(r.URL.Path)
	if redirect.Webroot != "" && strings.HasPrefix(p, "/.well-known/acme-challenge/") {
		// it's an ACME verification request -> serve file
		http.ServeFile(w, r, redirect.Webroot + p)
	} else {
		// it's something else that should probably be done via HTTPS -> redirect
		r.URL.Scheme = "https"
		r.URL.Host = r.Host
		if colon := strings.IndexByte(r.URL.Host, ':'); colon >= 0 {
			r.URL.Host = r.URL.Host[:colon]
		}
		http.RedirectHandler(r.URL.String(), http.StatusMovedPermanently).ServeHTTP(w, r)
	}
}
