package main

import "strings"

// Resolver is a type that can resolve strings.
type Resolver interface{
	Resolve(string) string
}

// Initializable is a type that can be initialized using the Init() method. If it is already initialized, that method
// should always return nil.
type Initializable interface{
	Init() error
}

// Resolve resolves a domain to an upstream target, using the Resolvers specified in the Proxy object.
func (this *Proxy) Resolve(domain string) string {
	// strip port
	colon := strings.LastIndexByte(domain,  ':')
	if colon >= 0 {
		domain = domain[:colon]
	}

	// try all resolvers until one matches
	upstream := ""
	for _, resolver := range this.Resolvers {
		if initializable, ok := resolver.(Initializable); ok {
			throw(initializable.Init(), true)
		}
		upstream = resolver.Resolve(domain)
		if upstream != "" {
			break
		}
	}
	return upstream
}
