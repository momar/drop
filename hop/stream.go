package main

import (
	"io"
	"net"
	"sync"
)

// StreamContext represents a connection that can be streamed to another upstream target (using the `ToUpstream` method).
// Until the upstream target has been set, data can be read and buffered in the `Leftover` field, which will be written.
// to the upstream when the connection has been established. This can e.g. be used to read & manipulate headers
type StreamContext struct{
	Conn net.Conn
	Data map[string]string
	Leftovers []byte
	HeaderInsertIndex int
	IsEOF bool
	working sync.RWMutex
}

// segmentSize is the size of "the smallest TCP segment that any TCP/IP host is required to support"
// (see section 2 on https://www.haproxy.org/download/1.8/doc/proxy-protocol.txt)
const segmentSize = 536

// NewStream creates a new `StreamContext` from an existing connection.
func NewStream(conn net.Conn) *StreamContext {
	return &StreamContext{
		Conn:      conn,
		Data:      map[string]string{},
		Leftovers: []byte{},
		HeaderInsertIndex: 0,
		IsEOF:     false,
		working:   sync.RWMutex{},
	}
}

// ToUpstream passes the connection to an upstream TCP target server without any buffering, starting with the (already
// buffered) contents of the `Leftover` field.
func (stream *StreamContext) ToUpstream(upstream string) error {
	// connect to the upstream target
	upstreamConn, err := net.Dial("tcp", upstream)
	if err != nil {
		return err
	}

	// write leftovers
	_, err = upstreamConn.Write(stream.Leftovers)
	if err != nil {
		return err
	}
	if stream.IsEOF {
		_ = upstreamConn.Close()
		return nil
	}

	// pipe the upstream connection to the original connection, and the other way around
	stream.working.RLock()
	stream.working.RLock()
	go stream.pipe(upstreamConn.(*net.TCPConn), stream.Conn.(*net.TCPConn))
	go stream.pipe(stream.Conn.(*net.TCPConn), upstreamConn.(*net.TCPConn))
	stream.working.Lock() // wait for the Mutex to be unlocked
	_ = upstreamConn.Close()
	return nil
}

// pipe calls to.ReadFrom(from) and then unlocks the `working` mutex of the stream.
func (stream *StreamContext) pipe(from *net.TCPConn, to *net.TCPConn) {
	// for the future: maybe hooks are required for specific byte sequences to allow full HTTP/2 support?
	_, err := to.ReadFrom(from)
	if err != io.EOF {
		throw(err, false)
	}
	_ = to.CloseWrite()
	_ = from.CloseRead()
	stream.working.RUnlock()
}
