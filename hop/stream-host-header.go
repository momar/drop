package main

import (
	"bytes"
	"errors"
	"io"
	"strings"
	"time"
)

var hostHeaderStart = []byte("\r\nHost:")
var hostHeaderEnd = []byte("\r\n")

// ParseHostHeader starts to read the stream & detects the Host header if sent within the first 10 seconds. It also sets
// `HeaderInsertIndex` to the start index of the line below the Host header, which can be used to insert additional headers.
func (stream *StreamContext) ParseHostHeader() error {
	// limit the read time for the Host header to 10 seconds; we will reset this at the end
	throw(stream.Conn.SetReadDeadline(time.Now().Add(10 * time.Second)), false)

	position := 0 // the current position in stream.Leftovers, as it can be different from its length when using Read()
	var startIndex = -1 // the start position of the value of the Host header, or -1 if it hasn't been found yet

	// helper variables for stream.Conn.Read
	var n int
	var err error

	// 8192 bytes is the header size limit imposed by most web servers (https://stackoverflow.com/a/8623061)
	for err != io.EOF && position < 8192 {
		// make stream.Leftovers fit another segment
		stream.Leftovers = append(stream.Leftovers, make([]byte, segmentSize - (len(stream.Leftovers) - position))...)
		// read another segment
		n, err = stream.Conn.Read(stream.Leftovers[position:])
		if err != nil && err != io.EOF {
			throw(stream.Conn.SetReadDeadline(time.Time{}), false)
			return err
		} else if err == io.EOF {
			stream.IsEOF = true
		}
		position += n
		// we haven't found the start position of the host header yet
		if startIndex == -1 {
			// startPos represents the start position of the search for the host header - as it can be split over TCP
			// segments (e.g. \r\nHo | st: ), we start len(hostHeaderStart) before the current position
			startPos := position - n - len(hostHeaderStart)
			if startPos < 0 {
				startPos = 0
			}
			if startIndex = bytes.Index(stream.Leftovers[startPos:position], hostHeaderStart); startIndex >= 0 {
				// we found the start index of our host header
				startIndex = startPos + startIndex + len(hostHeaderStart) // the start index of the header value
			}
		}
		if startIndex >= 0 {
			// try to find the end position of the host header
			endIndex := bytes.Index(stream.Leftovers[startIndex:], hostHeaderEnd)
			if endIndex >= 0 {
				// we found a host header! \o/
				stream.Data["Host"] = strings.TrimSpace(string(stream.Leftovers[startIndex:startIndex + endIndex]))
				stream.HeaderInsertIndex = startIndex + endIndex + len(hostHeaderEnd)

				// shorten stream.Leftovers so it only contains bytes actually read from the connection
				stream.Leftovers = stream.Leftovers[:position]
				throw(stream.Conn.SetReadDeadline(time.Time{}), false)
				return nil
			}
			// end of Host header is in next segment
		}
	}

	throw(stream.Conn.SetReadDeadline(time.Time{}), false)
	return errors.New("no Host header found")
}
