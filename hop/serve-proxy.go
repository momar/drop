package main

import (
	"net"
	"time"
)

// Proxy is a reverse proxy that resolves the upstream target using the provided Resolvers, and listens on the specified
// Address (defaulting to 127.0.0.1:8156).
type Proxy struct{
	Resolvers []Resolver
	Address string
	listener net.Listener
}

// Listen is a blocking method that starts the proxy on the specified address and accepts connections using the method
// proxy.Handle(net.Conn).
func (proxy *Proxy) Listen() {
	if proxy.Address == "" {
		proxy.Address = "127.0.0.1:8156"
	}

	var err error
	proxy.listener, err = net.Listen("tcp", proxy.Address)
	throw(err, true)

	debug("listening on " + proxy.Address + " as a reverse proxy")
	for {
		conn, err := proxy.listener.Accept()
		if throw(err, false) {
			continue
		}
		go proxy.Handle(conn)
	}
}

// Error immediately writes the HTTP status from "code" (which must contain both number and text representation,
// e.g. "400 Bad Request") to conn, and closes the connection afterwards.
func (proxy *Proxy) Error(conn net.Conn, code string) {
	throw(conn.SetWriteDeadline(time.Now().Add(10 * time.Second)), false)
	_, err := conn.Write([]byte("HTTP/1.1 " + code + "\r\nServer: hop\r\nContent-Type: text/plain\r\nConnection: close\r\n\r\n" + code))
	throw(err, false)
}

// Handle is the proxy's connection handler that creates a StreamContext from the connection, parses the PROXY protocol
// and the Host header, resolves the upstream target and then starts the stream between upstream and connection.
func (proxy *Proxy) Handle(conn net.Conn) {
	defer conn.Close()

	// TODO: HTTP/2 support?! How possible is it?
	stream := NewStream(conn)

	// parse & strip the PROXY v1/v2 protocol from the start of the stream
	throw(stream.ParseProxyProtocol(), false)

	// parse the Host header to resolve the upstream target
	// TODO: maybe it's possible & easier to check the SNI target from the PROXY protocol?
	//       also see 2.2.2 in https://www.haproxy.org/download/1.8/doc/proxy-protocol.txt
	err := stream.ParseHostHeader()
	if err != nil {
		if err.Error() != "no Host header found" {
			throw(err, false)
		}
		proxy.Error(conn, "400 Bad Request")
		return
	}

	// resolve the upstream target using the proxy's Resolvers
	upstream := proxy.Resolve(stream.Data["Host"])
	if upstream == "" {
		proxy.Error(conn, "501 Not Implemented")
		return
	}

	// stream the connection to the upstream target
	if throw(stream.ToUpstream(upstream), false) {
		proxy.Error(conn, "502 Bad Gateway")
		return
	}
}
