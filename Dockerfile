FROM golang:alpine AS hop
COPY hop /build
WORKDIR /build
RUN go build -ldflags '-s -w' -o hop .

FROM alpine:edge
RUN printf '@testing http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories &&\
    apk add --no-cache dumb-init docker-cli acme.sh hitch@testing
COPY init.sh /bin/
COPY --from=hop /build/hop /usr/local/bin/hop
COPY acme.sh /usr/local/bin/acme.sh
COPY hitch.cfg /etc/hitch.cfg
EXPOSE 80
EXPOSE 443
CMD ["/bin/init.sh"]
