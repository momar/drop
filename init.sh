#!/usr/bin/dumb-init /bin/ash

set -e
mkdir -p /data
[ -e /root/.acme.sh ] || ln -s /data /root/.acme.sh

[ -e /data/dhparam.pem ] || openssl dhparam -out /data/dhparam.pem 2048 # https://github.com/varnish/hitch/blob/master/docs/configuration.md

mkdir -p /data/.localhost
[ -e /data/.localhost/localhost.key ] || openssl req -x509 -newkey rsa:4096 -keyout /data/.localhost/localhost.cer -out /data/.localhost/localhost.key -days 36500 -subj '/CN=localhost' -nodes # https://stackoverflow.com/a/10176685

mkdir -p /data/bundles
[ -e /data/bundles/.localhost.pem ] || cat /data/.localhost/localhost.key /data/.localhost/localhost.cer /data/dhparam.pem > /data/bundles/.localhost.pem
[ -e /var/lib/hitch ] || mkdir /var/lib/hitch
chmod 700 /data/bundles
chown nobody:nobody /data/bundles /var/lib/hitch # TODO: run hitch as root?

mkdir -p /var/www
cd /data

# reverse proxy to terminate TLS
/usr/sbin/hitch --config=/etc/hitch.cfg &

# reverse proxy to resolve the domain names
/usr/local/bin/hop &

# auto-renew old certificates every 6 hours
{ while true; do /usr/bin/acme.sh --cron; sleep 21600; done; } &

wait -n
