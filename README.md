# drop

**drop** is a small Docker image built for a worry-free reverse proxy experience: run multiple HTTP containers on a single host, separate them by (sub-)domain names, and make them reachable from the internet via HTTPS powered by Let's Encrypt. Also, it's extremely efficient & can handle thousands of connections at once.

## Current State
**This project is currently at v0.1, and you probably shouldn't use it yet for production. I am though, and it seems to work reliable enough.**

## Components
- [**hitch**](https://hitch-tls.org): terminate HTTPS/TLS connections on :443 and forward them to *hop*.
- [**hop**](https://codeberg.org/momar/drop/src/branch/master/hop): proxy HTTP connections to their respective container just by their hostnames via plain TCP. Listens on :80 for HTTPS redirection and ACME webroot, and on 127.0.0.1:315 for proxy requests.
- [**acme.sh**](https://acme.sh): generates and automatically renews Let's Encrypt (or other ACME-based) certificates, and restart/reload hitch and other Docker containers.
- additionally, [Busybox](https://busybox.net) (as shipped with [Alpine Linux](https://alpinelinux.org)) as well as the [Docker](https://docker.com) CLI and [dumb-init](https://github.com/Yelp/dumb-init) is being used to hold everything together.

## Usage
<!-- 1.   - add `-e HSTS=63072000` somewhere before the last argument to force-enable [HTTP Strict Transport Security](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security), which tells the browser that no matter what only HTTPS should be used -->
<!-- 3.   - if multiple containers match a domain, a random one will be chosen for each request (effectively providing simple load balancing) -->
<!-- 3.   - if an upstream server doesn't respond within 30 seconds, it will get blacklisted for 5 minutes unless all other servers for this domain are currently blacklisted. -->
1. start the container:
   ```bash
   docker run -d --net=host -v /var/run/docker.sock:/var/run/docker.sock -v /var/drop:/data --name drop momar/drop
   ```
2. get a certificate:
   ```bash
   docker exec -it drop acme.sh --issue -d example.org -w /var/www
   ```
3. start another container on a specific domain:
   ```bash
   docker run --label drop.domain=test.example.org ...
   ```
   - if you want to use a different port than the one the container exposes, add e.g. `--label drop.port=8080` (if the container doesn't expose a port, port 80 is being used by default)
   - to proxy to a completely different arbitrary upstream target, use `docker run -d --label drop.host=example.org --label drop.upstream=example.org:80 momar/noop`
   - to redirect requests to another domain (e.g. www to non-www), you can use `docker run -d --label drop.host=www.example.org -e TO=https://example.org -e PERMANENT=1 momar/redirect`
   - to mount a certificate in another container (like an email or LDAP server that uses its own TLS port): `-v "/var/drop/example.org:..." --label drop.restart=true` (if your container supports reloading theough SIGHUP, replace `restart` with `reload`)
<!--   - the "X-Forwarded-*" and "Forwarded" headers will be set -->


## Roadmap
- [x] &nbsp;Directly proxy TCP connections based on their HTTP Host header
- [x] &nbsp;Terminate TLS connections and make it easy to use Let's Encrypt certificates
- [x] &nbsp;Find upstream target by Docker labels
- [x] &nbsp;Make everything maintainable & reliable **(v0.1)**
- [ ] &nbsp;Implement simple load balancing with health checks if there are multiple eligible upstream targets
- [x] &nbsp;~~Make permanent domain-wide redirects possible (e.g. www to non-www)~~
- [ ] &nbsp;Add basic request header manipulation (Forwarded IP)
- [ ] &nbsp;Add basic response header manipulation (HSTS)
- [ ] &nbsp;Path-based routing (e.g. example.org/test/* to localhost:5555/*)
- [ ] &nbsp;Select domains ~~or upstream targets~~ via regular expressions **(v1.0)**
- [ ] &nbsp;Support HTTP/2
- [ ] &nbsp;Support other configuration providers (like Kubernetes) **(v2.0)**